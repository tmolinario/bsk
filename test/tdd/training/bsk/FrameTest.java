package tdd.training.bsk;

import static org.junit.Assert.*;

import org.junit.Test;


public class FrameTest {

	@Test
	// User Story 1
	public void testFrame1() throws BowlingException{
		int firstThrow = 6;
		int secondThrow = 8;
		
		try {
			Frame f = new Frame(firstThrow, secondThrow);
			assertEquals(firstThrow, f.getFirstThrow());
			assertEquals(secondThrow, f.getSecondThrow());
		}catch(BowlingException b) {
			System.err.println(b.getMessage());
		}
		
	}
	
	@Test
	// User Story 2
	public void testFrame2() throws BowlingException{
		int firstThrow = 6;
		int secondThrow = 8;
		int score = firstThrow + secondThrow;
		
		try {
			Frame f = new Frame(firstThrow, secondThrow);
			assertEquals(score, f.getScore());
		}catch(BowlingException b) {
			System.err.println(b.getMessage());
		}
		
	}
	
	@Test
	// User Story 3
	public void testFrame3() throws BowlingException{
		try {
			Game g = new Game();
			g.addFrame(new Frame(5,6));
			g.addFrame(new Frame(1,2));
			g.addFrame(new Frame(6,8));
			g.addFrame(new Frame(5,7));
			g.addFrame(new Frame(8,1));
			g.addFrame(new Frame(5,6));
			g.addFrame(new Frame(5,6));
			g.addFrame(new Frame(9,1));
			g.addFrame(new Frame(1,9));
			g.addFrame(new Frame(5,5));
			assertEquals(new Frame(5,6).getFirstThrow(), g.getFrameAt(1).getFirstThrow());
			assertEquals(new Frame(5,6).getSecondThrow(), g.getFrameAt(1).getSecondThrow());
			
		}catch(BowlingException b) {
			System.err.println(b.getMessage());
		}

	}
	
	
	@Test
	// User Story 4
	public void testFrame4() throws BowlingException{
		try {
			Game g = new Game();
			g.addFrame(new Frame(5,4));
			g.addFrame(new Frame(1,2));
			g.addFrame(new Frame(6,1));
			g.addFrame(new Frame(5,1));
			g.addFrame(new Frame(8,1));
			g.addFrame(new Frame(5,1));
			g.addFrame(new Frame(5,1));
			g.addFrame(new Frame(9,0));
			g.addFrame(new Frame(1,5));
			g.addFrame(new Frame(5,2));
			
			
			assertEquals(68, g.calculateScore());
			
		}catch(BowlingException b) {
			System.err.println(b.getMessage());
		}

	}

	
	@Test
	// User Story 5
	public void testFrame5() throws BowlingException{
		try {
			Game g = new Game();
			g.addFrame(new Frame(5,4));
			g.addFrame(new Frame(1,2));
			g.addFrame(new Frame(6,3));
			g.addFrame(new Frame(6,4));
			g.addFrame(new Frame(8,1));
			g.addFrame(new Frame(5,2));
			g.addFrame(new Frame(5,0));
			g.addFrame(new Frame(9,0));
			g.addFrame(new Frame(1,0));
			g.addFrame(new Frame(5,3));
			
			//System.out.println(g.calculateScore());
			assertEquals(78, g.calculateScore());
			
		}catch(BowlingException b) {
			System.err.println(b.getMessage());
		}

	}

	@Test
	// User Story 6
	public void testFrame6() throws BowlingException{
		try {
			Game g = new Game();
			g.addFrame(new Frame(5,4));
			g.addFrame(new Frame(1,2));
			g.addFrame(new Frame(6,3));
			g.addFrame(new Frame(5,1));
			g.addFrame(new Frame(8,1));
			g.addFrame(new Frame(5,2));
			g.addFrame(new Frame(5,0));
			g.addFrame(new Frame(9,0));
			g.addFrame(new Frame(10,0));
			g.addFrame(new Frame(5,3));
			
			//System.out.println(g.calculateScore());
			assertEquals(83, g.calculateScore());
			
		}catch(BowlingException b) {
			System.err.println(b.getMessage());
		}

	}
	
	
	@Test
	// User Story 7
	public void testFrame7() throws BowlingException{
		try {
			Game g = new Game();
			g.addFrame(new Frame(10,0));
			g.addFrame(new Frame(4,6));
			g.addFrame(new Frame(7,2));
			g.addFrame(new Frame(3,6)); 
			g.addFrame(new Frame(4,4)); 
			g.addFrame(new Frame(5,3)); 
			g.addFrame(new Frame(3,3)); 
			g.addFrame(new Frame(4,5));
			g.addFrame(new Frame(8,1));
			g.addFrame(new Frame(2,6));
			
			//System.out.println(g.calculateScore());
			assertEquals(103, g.calculateScore());
			
		}catch(BowlingException b) {
			System.err.println(b.getMessage());
		}
	}
		
		@Test
		// User Story 8
		public void testFrame8() throws BowlingException{
			try {
				Game g = new Game();
				g.addFrame(new Frame(10,0));
				g.addFrame(new Frame(10,0));
				g.addFrame(new Frame(7,2));
				g.addFrame(new Frame(3,6)); 
				g.addFrame(new Frame(4,4)); 
				g.addFrame(new Frame(5,3)); 
				g.addFrame(new Frame(3,3)); 
				g.addFrame(new Frame(4,5));
				g.addFrame(new Frame(8,1));
				g.addFrame(new Frame(2,6));
				
				//System.out.println(g.calculateScore());
				assertEquals(112, g.calculateScore());
				
			}catch(BowlingException b) {
				System.err.println(b.getMessage());
			}
	}
	
		@Test
		// User Story 9
		public void testFrame9() throws BowlingException{
			try {
				Game g = new Game();
				g.addFrame(new Frame(8,2));
				g.addFrame(new Frame(5,5));
				g.addFrame(new Frame(7,2));
				g.addFrame(new Frame(3,6)); 
				g.addFrame(new Frame(4,4)); 
				g.addFrame(new Frame(5,3)); 
				g.addFrame(new Frame(3,3)); 
				g.addFrame(new Frame(4,5));
				g.addFrame(new Frame(8,1));
				g.addFrame(new Frame(2,6));
				
				//System.out.println(g.calculateScore());
				assertEquals(98, g.calculateScore());
				
			}catch(BowlingException b) {
				System.err.println(b.getMessage());
			}
	}
	
	@Test
	// User Story 10
	public void testFrame10() throws BowlingException{
		try {
			Game g = new Game();
			g.addFrame(new Frame(1,5));
			g.addFrame(new Frame(3,6));
			g.addFrame(new Frame(7,2));
			g.addFrame(new Frame(3,6)); 
			g.addFrame(new Frame(4,4)); 
			g.addFrame(new Frame(5,3)); 
			g.addFrame(new Frame(3,3)); 
			g.addFrame(new Frame(4,5));
			g.addFrame(new Frame(8,1));
			g.addFrame(new Frame(2,8));
			g.setFirstBonusThrow(7);
			
			//System.out.println(g.calculateScore());
			assertEquals(90, g.calculateScore());
			
		}catch(BowlingException b) {
			System.err.println(b.getMessage());
		}
	}
	
	@Test
	// User Story 11
	public void testFrame11() throws BowlingException{
		try {
			Game g = new Game();
			g.addFrame(new Frame(1,5));
			g.addFrame(new Frame(3,6));
			g.addFrame(new Frame(7,2));
			g.addFrame(new Frame(3,6)); 
			g.addFrame(new Frame(4,4)); 
			g.addFrame(new Frame(5,3)); 
			g.addFrame(new Frame(3,3)); 
			g.addFrame(new Frame(4,5));
			g.addFrame(new Frame(8,1));
			g.addFrame(new Frame(10,0));
			g.setFirstBonusThrow(7);
			g.setSecondBonusThrow(2);
			
			//System.out.println(g.calculateScore());
			assertEquals(92, g.calculateScore());
			
		}catch(BowlingException b) {
			System.err.println(b.getMessage());
		}
	}
	
	@Test
	// User Story 12
	public void testFrame12() throws BowlingException{
		try {
			Game g = new Game();
			g.addFrame(new Frame(10,0));
			g.addFrame(new Frame(10,0));
			g.addFrame(new Frame(10,0));
			g.addFrame(new Frame(10,0));
			g.addFrame(new Frame(10,0));
			g.addFrame(new Frame(10,0));
			g.addFrame(new Frame(10,0));
			g.addFrame(new Frame(10,0));
			g.addFrame(new Frame(10,0));
			g.addFrame(new Frame(10,0));
			g.setFirstBonusThrow(10);
			g.setSecondBonusThrow(10);
			
			//System.out.println(g.calculateScore());
			assertEquals(300, g.calculateScore());
			
		}catch(BowlingException b) {
			System.err.println(b.getMessage());
		}
	}
	
	
}
